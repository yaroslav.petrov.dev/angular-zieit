# Docker installation

Make sure that docker and docker-compose are installed on your computer

After that, follow the instruction:

1. `docker-compose up --build`
2. Wait until containers are builded
3. Open `http://localhost:4200`
4. Enjoy:)

Of course, if docker is not convenient - you can just launch it by your local node/angular